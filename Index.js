const _navToggle = document.querySelector('.navigationButton');
const _navLinks = document.querySelectorAll('.nav_link');

_navToggle.addEventListener('click', () =>{
    document.body.classList.toggle('navigation_open');
})

_navLinks.forEach(link =>{
    link.addEventListener('click', () =>{
        document.body.classList.remove('navigation_open');
    })
})